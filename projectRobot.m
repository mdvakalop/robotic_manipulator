%D-H parameters
syms ti ai di gi;   %ti = theta_i, gi = alpha_i

%qi variables and lengths
syms q1 q2 q3;
syms l0 l1 l2 l3;


%% 1. (Denavit-Hartenberg array and parameters)

A = [cos(ti) -sin(ti)*cos(gi) sin(ti)*sin(gi) ai*cos(ti);
    sin(ti) cos(ti)*cos(gi) -cos(ti)*sin(gi) ai*sin(ti);
    0 sin(gi) cos(gi) di;
    0 0 0 1];

DH = [0 l0 pi/2 0 
      q1 0 -pi/2 0
      q2 l1 0 l2
      q3 0 0 l3];
  
  

%% 2. (Kinematic analysis)

%calculate A00', A0'1, A12, A2E
A00t = subs(A, [ti, di, gi, ai], DH(1,:));
A0t1 = subs(A, [ti, di, gi, ai], DH(2,:));
A12 = subs(A, [ti, di, gi, ai], DH(3,:));
A2E = subs(A, [ti, di, gi, ai], DH(4,:));

%calculate A01, A02, A0E (used in b-computation at 3.)
A01 = simplify(A00t*A0t1);
A02 = simplify(A01*A12);
A1E = simplify(A12*A2E);  %for proper simplify at A0E
A0E = (simplify(simplify(A00t*A0t1)*A1E));


%% 3. (Jacobian - differential analysis)

b = [0 0 1]';
b0 = b;
b0t = A00t(1:3,1:3)*b;
b1 = A01(1:3,1:3)*b; % A01(1:3,1:3) = R01
b2 = A02(1:3,1:3)*b; % A02(1:3,1:3) = R02

r0E = A0E(1:3,4);
r0tE = simplify(r0E - A00t(1:3,4));
r1E = simplify(r0E - A01(1:3,4));
r2E = simplify(r0E - A02(1:3,4));

JL1 = diff(r0E, q1);
%elseJL1 = diff (r0tE, q1)same with above because from syst0 to syst0' only
%with constants (l0,pi..)
%elseJL1 = cross(b0, r0E) %these 2 NOT same cause q1 is in 0t system
%elseJL1 = cross(b0t, r0tE); %these 2 are same (OK)

JL2 = diff(r1E, q2); %these 2 are same (OK)
%elseJL2 = simplify(cross(b1, r1E));

JLE = diff(r2E, q3); %these 2 are same (OK)
%elseJLE = simplify(cross(b2, r2E));

JL = [JL1 JL2 JLE];
JA = [b0t b1 b2];
J = [JL;JA];


%% 4. (Pseudoinverse Jacobian - Inverse differential analysis)

j0 = simplify(J*J');
j1 = inv(j0);
%Jplus = simplify((J')*j1)
Jplus = simplify((J')/(simplify(J*J'))); %pseudoinverse J

%detJL = simplify(det(JL))
eqnSingul = l3*cos(q2+q3)+l2*cos(q2) == 0; %singularity computation
SSingul = solve(eqnSingul, [q2 q3]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 6. Trajectories - calculations of ai's for polynomials
syms t d Bx Ax %t == T and d == �
syms a10 a13 a14
syms a20 a21
syms a0 a1 a2 a3 a4 %these should be a3i

%phase 1, a_i's 
a10 = Ax; 
a13 = (Bx-Ax)/(20*d^2*(t-2*d));
a14 = (Bx-Ax)/(80*d^3*(t-2*d)); 


%phase 2, a_i's
a20 = Ax+(-Bx+Ax)*7*d/(5*(t-2*d));
a21 = (Bx-Ax)/(t-2*d);
            
%3rd PHASE
x3TD = simplify(a0 + a1*(t-2*d) + a2*(t-2*d)^2 + a3*(t-2*d)^3 + a4*(t-2*d)^4);
x3T = a0 + a1*(t) + a2*(t)^2 + a3*(t)^3 + a4*(t)^4;
edx3TD = (Bx-Ax)/(t-2*d);
ex3TD = (Ax*d + Bx*(t-3*d))/(t-2*d);

%eqn2 = ex3TD == a0 + a1*(t-2*d) + a2*(t-2*d)^2 + a3*(t-2*d)^3 + a4*(t-2*d)^4; 
% only 5 equations needed for 5 variables.
eqn2 = Bx == a0 + a1*(t) + a2*(t)^2 + a3*(t)^3 + a4*(t)^4; %ex3T
eqn1t = edx3TD == simplify(diff(x3TD, t));
eqn2t = 0 == simplify(diff(x3T, t)); %edx3T
eqn1tt = 0 == simplify(diff(diff(x3TD, t), t)); %eddx3TD
eqn2tt = 0 == simplify(diff(diff(x3T, t), t)); %eddx3T
eqnTraj = [eqn2, eqn1t, eqn2t, eqn1tt, eqn2tt];
a = [a0 a1 a2 a3 a4];
STraj = solve(eqnTraj, a); %use S.ai to see solition for ai
a0 = STraj.a0; %these prices are for general T,�,Ax,Bx.
a1 = STraj.a1;
a2 = STraj.a2;
a3 = STraj.a3;
a4 = STraj.a4;

Tf = 3.0;
delta = 0.3;
%initial/final end-point position --> desired task-space trajectory  
xd0 = 5.0; %initial point Ax
xd1 = 2.0; %final point Bx
yd0 = 0.0; 
yd1 = 5.0; 
zd0 = 5.0;
zd1 = 5.0; %zd remains 5 the whole time

%compute real value a_i's for px
%phase 1
a10val = subs(a10, Ax, xd0); 
a13val = subs(a13, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
a14val = subs(a14, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
%phase 2
a20val = subs(a20, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
a21val = subs(a21, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
%phase 3
a0val = subs(a0, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
a1val = subs(a1, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
a2val = subs(a2, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
a3val = subs(a3, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);
a4val = subs(a4, [t, d, Ax, Bx], [Tf, delta, xd0, xd1]);

l0 = 2.0;
l1 = 2.0;
l2 = 4.0;
l3 = 4.0;
dt = 0.001;
 
tt1=0:dt:2*delta;
tt2=2*delta+dt:dt:Tf-2*delta;
tt3=Tf-2*delta+dt:dt:Tf;

px1 = a10val + a13val*tt1.^3 + a14val*tt1.^4;
px2 = a20val + a21val*tt2;
px3 = a0val + a1val*tt3 + a2val*tt3.^2 + a3val*tt3.^3 + a4val*tt3.^4;
px = [px1 px2 px3];

%velocity vx
tt1=0:dt:2*delta;
tt2=2*delta+dt:dt:Tf-2*delta;
[ll, lentt2] = size(tt2);
tt3=Tf-2*delta+dt:dt:Tf;

vx1 = 3*a13val*tt1.^2 + 4*a14val*tt1.^3;
vx2 = a21val*ones(1,lentt2);
vx3 = a1val + 2*a2val*tt3 + 3*a3val*tt3.^2 + 4*a4val*tt3.^3;
vx = [vx1 vx2 vx3];


%compute real value a_i's for py
%phase 1
a10val = subs(a10, Ax, yd0); 
a13val = subs(a13, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
a14val = subs(a14, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
%phase 2
a20val = subs(a20, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
a21val = subs(a21, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
%phase 3
a0val = subs(a0, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
a1val = subs(a1, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
a2val = subs(a2, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
a3val = subs(a3, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);
a4val = subs(a4, [t, d, Ax, Bx], [Tf, delta, yd0, yd1]);

tt1=0:dt:2*delta;
tt2=2*delta+dt:dt:Tf-2*delta;
tt3=Tf-2*delta+dt:dt:Tf;

py1 = a10val + a13val*tt1.^3 + a14val*tt1.^4;
py2 = a20val + a21val*tt2;
py3 = a0val + a1val*tt3 + a2val*tt3.^2 + a3val*tt3.^3 + a4val*tt3.^4;
py = [py1 py2 py3];

%velocity vy
tt1=0:dt:2*delta;
%tt2=2*delta+dt:dt:Tf-2*delta;
%[ll, lentt2] = size(tt2);
tt3=Tf-2*delta+dt:dt:Tf;

vy1 = 3*a13val*tt1.^2 + 4*a14val*tt1.^3;
vy2 = a21val*ones(1,lentt2);
vy3 = a1val + 2*a2val*tt3 + 3*a3val*tt3.^2 + 4*a4val*tt3.^3;
vy = [vy1 vy2 vy3];

%pz
[ll, len] = size(px);
pz = zd0*ones(1, len);
%vz
vz = zeros(1,len);

tt=0:dt:Tf;
%tranjectories
figure(1);
subplot(3,1,1)
plot(tt,px) %plot pEx
ylabel('p_E_,_x (cm)'); 
xlabel('time t (sec)');
ylim([-1 6]);
subplot(3,1,2)
plot(tt,py) %plot pEy
ylabel('p_E_,_y (cm)'); 
xlabel('time t (sec)');
ylim([-1 6]);
subplot(3,1,3)
plot(tt,pz) %plot pEz
ylabel('p_E_,_z (cm)'); 
xlabel('time t (sec)');
ylim([-1 6]);

tt=0:dt:Tf;
%velocities
figure(2);
subplot(3,1,1)
plot(tt,vx); %plot vEx
ylabel('v_E_,_x (cm)'); 
xlabel('time t (sec)');
%ylim([-1 6]);
subplot(3,1,2)
plot(tt,vy); %plot vEy
ylabel('v_E_,_y (cm)'); 
xlabel('time t (sec)');
%ylim([-1 6]);
subplot(3,1,3)
plot(tt,vz); %plot vEz=0
ylabel('v_E_,_z (cm)'); 
xlabel('time t (sec)');


%% inverse model - angles qi
rd1 = sqrt((pz(:)- l0).^2 + px(:).^2);
qd(:,1) = -acos(px(:)./rd1(:)) + asin(-l1./rd1(:));

qd(:,3) = acos((px(:).^2 + py(:).^2 + (pz(:)-l0).^2 -l1^2 -l2^2 -l3^2)/(2*l2*l3));
%qd(:,3) = [qd3(:) -qd3(:)]';

c3 = cos(qd(:,3));
rd2 = sqrt(l2^2 + l3^2 + 2*l2*l3.*c3(:));
qd(:,2) = -acos((l2 + l3*c3(:))./rd2(:)) + asin(py(:)./rd2(:));

tt=0:dt:Tf;
%angles 
figure(3);
subplot(3,1,1)
plot(tt,qd(:,1)); %plot q1
ylabel('q_1 (rad)'); 
xlabel('time t (sec)');
subplot(3,1,2)
plot(tt,qd(:,2)); %plot q2
ylabel('q_2 (rad)'); 
xlabel('time t (sec)');
subplot(3,1,3)
plot(tt,qd(:,3)); %plot q3
ylabel('q_3 (rad)'); 
xlabel('time t (sec)');

%% inverse model - angular velocities (dot qi)

%Jplusval(1,1) = subs(Jplus(1,1), [q1, q2, q3], [qd(:,1), qd(:,2), qd(:,3)]);
[ll, lenvy] = size(vy);
Jplusval(1,:) = -sin(qd(:,1))./(l3*cos(qd(:,2)+qd(:,3)));%J+(1,1)
Jplusval(2,:) = zeros(1,lenvy);%J+(1,2)
Jplusval(3,:) = cos(qd(:,2)+qd(:,3)).*(l3*cos(qd(:,1)).*cos(qd(:,2)+qd(:,3))-l1*sin(qd(:,1))+l2*cos(qd(:,1)).*cos(qd(:,2)))./(l2*sin(qd(:,3)).*(l3*cos(qd(:,2)+qd(:,3))+l2*cos(qd(:,2))));%J+(2,1)
Jplusval(4,:) = sin(qd(:,2)+qd(:,3))./(l2*sin(qd(:,3)));%J+(2,2)
%Jplusval(5,:) = 

%%
dotqd(:,1) = Jplusval(1,:).*vx(:) + Jplusval(2,:).*vy(:);
dotqd(:,2) = Jplusval(3,:).*vx(:) + Jplusval(4,:).*vy(:);
%dotqd(:,3) = Jplusval(3,1).*vx(:) + Jplusval(3,2).*vy(:);

tt=0:dt:Tf;
%dot angles 
figure(4);
subplot(3,1,1)
plot(tt,dotqd(:,1)); %plot q1
ylabel('diff(q_1) (rad)'); 
xlabel('time t (sec)');
subplot(3,1,2)
plot(tt,dotqd(:,2)); %plot q2
ylabel('diff(q_2) (rad)'); 
xlabel('time t (sec)');
subplot(3,1,3)
%plot(tt,dotqd(:,3)); %plot q3
ylabel('diff(q_3) (rad)'); 
xlabel('time t (sec)');

%% Simulation
%%(xd0, yd0, zd0) : cartesian position of the 0-th link local reference fr(stable)
kmax=Tf/dt + 1; 
xd0(1:kmax) = 0;
yd0(1:kmax) = 0;
zd0(1:kmax) = l0;

%%(xd1, yd1, yd1) : cartesian position of the 1st link's local reference frame 
xd1 = -l1*sin(qd(:,1));   
yd1(1:kmax) = 0;
zd1 = l0 + l1*cos(qd(:,1));
%%(xd2, yd2, zd2) : cartesian position of the 2nd link's local reference frame 
xd2 = -l1*sin(qd(:,1)) + l2*cos(qd(:,1)).*cos(qd(:,2));
yd2 = l2*sin(qd(:,2));
zd2 = l0 + l1*cos(qd(:,1)) + l2*cos(qd(:,2)).*sin(qd(:,1));
%%(xd3, yd3, zd3) : cartesian position of the 3nd link's local reference frame 
xd3 = -l1*sin(qd(:,1)) + l2*cos(qd(:,1)).*cos(qd(:,2))+ l3*cos(qd(:,1)).*cos(qd(:,2)+qd(:,3));
yd3 = l2*sin(qd(:,2)) + l3*sin(qd(:,2)+qd(:,3));
zd3 = zd2(:) + l3*sin(qd(:,1)).*cos(qd(:,2)+qd(:,3));

figure(5); 
axis([-5 6 -6 8 -5 8])
axis on 
hold on 
xlabel('x (cm)'); 
ylabel('y (cm)'); 
zlabel('z (cm)');
plot3(px,py,pz,'rs'); 
dtk=1000; %% plot robot position every dtk samples, to animate its motion 
plot([0],[0],'o'); 
while(1)
for tk=1:dtk:kmax,  	
   pause(0.1);	%% pause motion to view successive robot configurations
   plot3([0,xd0(tk)],[0,yd0(tk)],[0,zd0(tk)]); % for the l0
   plot3([xd0(tk),xd1(tk)],[yd0(tk),yd1(tk)],[zd0(tk),zd1(tk)]); % here we have plotted the angle from 0 to joint 1
   plot3([xd1(tk)],[yd1(tk)],[zd1(tk)],'o'); % make joint 1 as an 'o'
   plot3([xd1(tk),xd2(tk)],[yd1(tk),yd2(tk)],[zd1(tk),zd2(tk)]); % plot the line between joint 1 and joint 2
   plot3([xd2(tk)],[yd2(tk)],[zd2(tk)],'y*'); % plot joint 2 as a '*'
   plot3([xd2(tk),xd3(tk)],[yd2(tk),yd3(tk)],[zd2(tk),zd3(tk)]); % plot the line between joint 2 and joint 3
   plot3([xd3(tk)],[yd3(tk)],[zd3(tk)],'md'); % plot joint 3 as a diamond
   plot3([xd3(tk),px(tk)],[yd3(tk),py(tk)],[zd3(tk),pz(tk)]); % plot the line between joint 3 and ending point
   plot3([px(tk)],[py(tk)],[pz(tk)],'g+'); % plot ending point as a +
end 
xd0=fliplr(xd0);
yd0=fliplr(yd0);
zd0=fliplr(zd0);
xd1=fliplr(xd1);
yd1=fliplr(yd1);
zd1=fliplr(zd1);
xd2=fliplr(xd2);
yd2=fliplr(yd2);
zd2=fliplr(zd2);
xd3=fliplr(xd3);
yd3=fliplr(yd3);
zd3=fliplr(zd3);
px=fliplr(px);
py=fliplr(py);
end
hold off

%Press Ctrl-c to end procedure.









